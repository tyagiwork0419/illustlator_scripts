﻿///<reference types="types-for-adobe/Illustrator/2015.3"


import { SettingDialog, InputData, Control } from "../SettingDialog/SettingDialog";
import "extendscript-es5-shim-ts";


class CustomTrimMark {
    doc: Document;
    color : RGBColor;
    locked: boolean;
    stroke: number;
    drawVerticalCenter: boolean;
    drawBesideCenter: boolean;

    constructor(document: Document, color: RGBColor, locked : boolean = true, stroke: number = 0.3, drawVerticalCenter: boolean = false, drawBesideCenter: boolean = false) {
        this.doc = document;
        this.color = color;
        this.locked = locked;
        this.stroke = stroke;
        this.drawVerticalCenter = drawVerticalCenter;
        this.drawBesideCenter = drawBesideCenter;
    };

    draw() {
        var group = this.doc.groupItems.add();
        group.locked = this.locked;
        var pathItems = group.pathItems;
        var selObj = this.doc.selection;
        if (selObj.length < 1) {
            alert("nothing is selected");
            throw new Error("何も選択されていません");
        }
        var rect = app.activeDocument.selection[0].geometricBounds;
        var x1 = rect[0];
        var y1 = rect[1];
        var x2 = rect[2];
        var y2 = rect[3];
        var d = 20; //線の長さ
        var value: number = 8;
        var o = value + this.stroke;

        //日本式トンボ
        this._drawCorner(pathItems, o, d, x1, y1, x2, y2);

        if (this.drawVerticalCenter) {
            this._drawVerticalCenter(pathItems, o, d, x1, y1, x2, y2);
        }

        if (this.drawBesideCenter) {
            this._drawBesideCenter(pathItems, o, d, x1, y1, x2, y2);
        }

    }

    _drawCorner(pathItems: PathItems, o: number, d: number, x1: number, y1: number, x2: number, y2: number) {
        this._drawLine3(pathItems, x1 - (d * 1.7), y1 + o, x1, y1 + o, x1, y1 + o + (d * 1.3)) //左上縦
        this._drawLine3(pathItems, x1 - (d * 1.7), y1 - o + o, x1 - o, y1 - o + o, x1 - o, y1 + o + (d * 1.3)) //左上縦

        this._drawLine3(pathItems, x2, y1 + o + (d * 1.3), x2, y1 + o, x2 + (d * 1.7), y1 + o) //右上縦
        this._drawLine3(pathItems, x2 + o, y1 + o + (d * 1.3), x2 + o, y1 + o - o, x2 + (d * 1.7), y1 + o - o) //左上横

        this._drawLine3(pathItems, x1 - (d * 1.7), y2 - o, x1, y2 - o, x1, y2 - o - (d * 1.3)) //左下縦
        this._drawLine3(pathItems, x1 - (d * 1.7), y2, x1 - o, y2, x1 - o, y2 - o - (d * 1.3)) //左下横

        this._drawLine3(pathItems, x2, y2 - o - (d * 1.3), x2, y2 - o, x2 + (d * 1.7), y2 - o) //右下縦
        this._drawLine3(pathItems, x2 + o, y2 - o - (d * 1.3), x2 + o, y2 + o - o, x2 + (d * 1.7), y2 + o - o) //左下横
    }

    _drawCenter(pathItems: PathItems, o: number, d: number, x1: number, y1: number, x2: number, y2: number){
        this._drawBesideCenter(pathItems, o, d, x1, x2, y1, y2);
        this._drawVerticalCenter(pathItems, o, d, x1, x2, y1, y2);
    }

    _drawVerticalCenter(pathItems: PathItems, o: number, d: number, x1: number, y1: number, x2: number, y2: number){
        this._drawLine2(pathItems, (x1 + x2) / 2 - (o * 4.4), y1 + (o * 2.2), (x1 + x2) / 2 + (o * 4.4), y1 + (o * 2.2)) //中央上横
        this._drawLine2(pathItems, (x1 + x2) / 2, y1 + (o * 1.5), (x1 + x2) / 2, y1 + o + (d * 1.4)) //中央上縦

        this._drawLine2(pathItems, (x1 + x2) / 2 - (o * 4.4), y2 - (o * 2.2), (x1 + x2) / 2 + (o * 4.4), y2 - (o * 2.2)) //中央下横
        this._drawLine2(pathItems, (x1 + x2) / 2, y2 - (o * 1.5), (x1 + x2) / 2, y2 - o - (d * 1.4)) //中央下縦
    }

    _drawBesideCenter(pathItems: PathItems, o: number, d: number, x1: number, y1: number, x2: number, y2: number){
        this._drawLine2(pathItems, x1 - (d * 1.8), (y1 + y2) / 2, x1 - (d * 0.6), (y1 + y2) / 2) //中央左横
        this._drawLine2(pathItems, x1 - (d * 0.9), (y1 + y2) / 2 + (o * 4.4), x1 - (d * 0.9), (y1 + y2) / 2 - (o * 4.4)) //中央左横

        this._drawLine2(pathItems, x2 + (d * 1.8), (y1 + y2) / 2, x2 + (d * 0.6), (y1 + y2) / 2) //中央右横
        this._drawLine2(pathItems, x2 + (d * 0.9), (y1 + y2) / 2 + (o * 4.4), x2 + (d * 0.9), (y1 + y2) / 2 - (o * 4.4)) //中央右横
    }

    // 線を描画(始点・終点の座標・2点)
    _drawLine2(pathItems: PathItems, sx: number, sy: number, ex: number, ey: number) {
        var pObj = pathItems.add();
        pObj.setEntirePath([
            [sx, sy],
            [ex, ey]
        ]);
        pObj.filled = false; //　塗りなし
        pObj.stroked = true; //　線あり
        pObj.strokeWidth = this.stroke; //　線幅1ポイント
        pObj.strokeColor = this.color; //　線の色を指定
        //pObj.guides = true;
    }

    // 線を描画(始点・終点の座標・3点)
    _drawLine3(pathItems: PathItems, x1: number, y1: number, x2: number, y2: number, x3: number, y3: number) {
        var pObj = pathItems.add();
        pObj.setEntirePath([
            [x1, y1],
            [x2, y2],
            [x3, y3]
        ]);
        pObj.filled = false; //　塗りなし
        pObj.stroked = true; //　線あり
        pObj.strokeWidth = this.stroke; //　線幅1ポイント
        pObj.strokeColor = this.color; //　線の色を指定
        //pObj.guides = true;
    }
    // カラーを設定
    _setColor(r: number, g: number, b: number) {
        var tmpColor = new RGBColor();
        tmpColor.red = r;
        tmpColor.green = g;
        tmpColor.blue = b;
        return tmpColor;
    }
}

let settings: InputData[] = [
    new InputData("color", "色", Control.DropDownList, "1"),
    new InputData("stroke", "線の太さ", Control.EditText, "0.3"),
    new InputData("locked", "ロック", Control.Checkbox, true),
    new InputData("drawVerticalCenter", "縦の中心トンボ", Control.Checkbox, true),
    new InputData("drawBesideCenter", "横の中心トンボ", Control.Checkbox, false),
]



function main() {
    var onClickOk = function (data: { [key: string]: any }) {
        const doc = app.activeDocument;

        //let message: string = "";
        //Object.keys(data).forEach(key => {
        //    message += data[key] + ", \n";

        //});
        //alert(message);

        const color = new RGBColor();
        color.red = 0;
        color.green = 0;
        color.blue = 0;
        const stroke: number = parseFloat(data["stroke"]);
        const locked = data["locked"];
        const drawVerticalCenter = data["drawVerticalCenter"];
        const drawBesideCenter = data["drawBesideCenter"];

        try{
            const trimMark = new CustomTrimMark(doc, color, locked, stroke, drawVerticalCenter, drawBesideCenter);
            trimMark.draw();
        }catch(e){
            alert("error");
            if(e instanceof Error){
                alert(e.toString());

            }

        }
    }

    var dialog = SettingDialog.create("title", settings, onClickOk);
    dialog.show();
}

main();