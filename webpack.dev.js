'use strict';
const webpack = require('webpack');
const { merge } = require('webpack-merge');
const common = require('./webpack.common');
const ES3Plugin = require("webpack-es3-plugin");
const BomPlugin = require('webpack-utf8-bom');

module.exports =
    //console.log('env.name = ' + env.name);
    merge(common, {
        mode: 'development',
        devtool: 'source-map',
        plugins: [
            new ES3Plugin(),
            new BomPlugin(true)
            //new webpack.DefinePlugin({
            //    DEBUG: true
            //})
        ]
    });
